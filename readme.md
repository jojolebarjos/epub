
# Minimalist EPUB Reader

This package provide minimal tools to extract HTML content from [EPUB](https://en.wikipedia.org/wiki/EPUB) files.


## Installation

Install from source:

```
pip install git+https://gitlab.com/jojolebarjos/epub.git
```


## Getting started

The most straightforward way to extract content is using the `to_html` method. The following example shows how to get a single HTML file as a binary string:

```python
from epub import EPUB

with open('my.epub', 'rb') as file:
    epub = EPUB(file)
    html = epub.to_html()
    print(html)
```

A `file` and a `path` arguments are available to export directly to disk:

```python
from epub import EPUB

with open('my.epub', 'rb') as file:
    epub = EPUB(file)
    epub.to_html(path='my.html')
```

For advanced usage, you might want to use the `HTMLRenderer` class directly.


## Changelog

 * 0.1.0 - 2020-06-07
    * Refactor and clean repository 
 * 0.0.1 - 2019-10-31
    * Initial release
