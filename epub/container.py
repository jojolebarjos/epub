
# Useful links:
#   https://en.wikipedia.org/wiki/EPUB
#   https://www.w3.org/publishing/epub3/epub-ocf.html
#   http://www.idpf.org/epub/20/spec/OPF_2.0.1_draft.htm
#   http://www.idpf.org/epub/dir/
#   http://www.ideal-group.org/cris-archives/sample_epub3_ebooks.htm


import io
import os
import zipfile

from lxml import etree


class Item:
    def __init__(self, file, id, href, mime_type, fallback_id=None):
        self.file = file
        self.id = id
        self.href = href
        self.mime_type = mime_type
        self.fallback_id = fallback_id

    @property
    def fallback(self):
        return self.file.manifest.get(self.fallback_id)

    def open(self, mode='rb', encoding=None):
        return self.file.open(self.href, mode, encoding)


class Itemref:
    def __init__(self, file, id, linear=True):
        self.file = file
        self.id = id
        self.linear = linear

    @property
    def item(self):
        return self.file.manifest.get(self.id)


class EPUB:
    def __init__(self, file):
        self._zip = zipfile.ZipFile(file)
        self._load_container()
        self._load_descriptor()

    def _load_container(self):
        """Parse metainfo to acquire OPF descriptor path."""

        # Load XML
        with self._zip.open('META-INF/container.xml', 'r') as container:
            tree = etree.parse(container)
            root = tree.getroot()

            # Handle dirty namespace stuff
            nsmap = dict(root.nsmap)
            nsmap['x'] = nsmap[None] # urn:oasis:names:tc:opendocument:xmlns:container
            del nsmap[None]

            # Get path of the first (default) rendition
            root_paths = root.xpath('//x:rootfile/@full-path', namespaces=nsmap)
            root_path = root_paths[0]
            root_folder = os.path.dirname(root_path)
            if len(root_folder) > 0 and not root_folder.endswith('/'):
                root_folder += '/'

            # Update attributes
            self._root_path = root_path
            self._root_folder = root_folder

    def _load_descriptor(self):
        """Parse package descriptor."""

        # Load XML
        with self._zip.open(self._root_path, 'r') as descriptor:
            tree = etree.parse(descriptor)
            root = tree.getroot()

            # Check version
            assert root.nsmap[None] == 'http://www.idpf.org/2007/opf'
            assert root.tag == '{http://www.idpf.org/2007/opf}package'
            # TODO assert root.attrib['version'] == '2.0'

            # Define relevant namespaces
            nsmap = {
                'opf': 'http://www.idpf.org/2007/opf',
                'dc': 'http://purl.org/dc/elements/1.1/'
            }

            # Get relevant metadata
            # TODO identifier (ISBN, IMRID...)
            self.titles = root.xpath('//dc:title/text()', namespaces=nsmap)
            self.languages = root.xpath('//dc:language/text()', namespaces=nsmap)
            # TODO publisher, author...
            # TODO dates
            # TODO description

            # Parse manifest and store items
            manifest = {}
            for e in root.xpath('opf:manifest/opf:item', namespaces=nsmap):
                id = e.attrib['id']
                href = e.attrib['href']
                mime_type = e.attrib['media-type']
                fallback = e.attrib.get('fallback')
                item = Item(self, id, href, mime_type, fallback)
                manifest[id] = item

            # Read spine
            spine = []
            for e in root.xpath('opf:spine/opf:itemref', namespaces=nsmap):
                id = e.attrib['idref']
                linear = e.attrib.get('linear') != 'no'
                itemref = Itemref(self, id, linear)
                spine.append(itemref)

            # Update attributes
            self.manifest = manifest
            self.spine = spine

    def open(self, href, mode='rb', encoding=None, newline=None):
        if not os.path.isabs(href):
            href = os.path.join(self._root_folder, href)
        file = self._zip.open(href, 'r')
        if 'b' not in mode:
            file = io.TextIOWrapper(file, encoding=encoding, newline=newline)
        return file

    def to_html(self, *, path=None, file=None, **kwargs):
        """Simple helper that uses default HTML renderer."""

        from .renderer import HTMLRenderer
        renderer = HTMLRenderer(**kwargs)
        if path is None and file is None:
            file = io.BytesIO()
            renderer.write(self, file)
            data = file.getvalue()
            return data
        if file is None:
            with io.open(path, 'wb') as file:
                renderer.write(self, file)
        else:
            if path is not None:
                raise ValueError('cannot specify both path and file arguments')
            renderer.write(self, file)
