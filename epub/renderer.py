
from lxml import etree, html
from lxml.builder import E

from bs4 import UnicodeDammit


def parse_html(file):
    raw = file.read()
    dammit = UnicodeDammit(raw, is_html=True)
    parser = html.HTMLParser(encoding=dammit.original_encoding)
    tree = html.document_fromstring(raw, parser=parser)
    return tree


class HTMLRenderer:
    def __init__(self, ignore_images=False):
        self.ignore_images = ignore_images

    def write(self, epub, file):

        head = []
        body = []

        # Force UTF-8
        head.append(E.meta(charset='UTF-8'))

        # Use only first title
        if epub.titles:
            head.append(E.title(epub.titles[0]))

        # TODO timestamp, author, description

        # Create body from spine
        # Note: non-linear items should be ignored (they are usually accessed indirectly, for instance through hyperlinks)
        for itemref in epub.spine:
            if itemref.linear:
                self._handle_item(head, body, itemref.item)

        # Assemble document
        # TODO language, see https://stackoverflow.com/a/21725828
        root = E.html(
            E.head(*head),
            E.body(*body),
        )

        # Write document
        data = html.tostring(root, pretty_print=True, encoding='utf-8')
        file.write(data)

    def _handle_item(self, head, body, item):
        # TODO image/gif
        # TODO image/jpeg
        # TODO image/png
        # TODO image/svg+xml
        if item.mime_type == 'application/xhtml+xml':
            return self._handle_xhtml(head, body, item)
        # TODO application/x-dtbook+xml
        # TODO text/css
        # TODO application/xml
        # TODO text/x-oeb1-document
        # TODO text/x-oeb1-css
        # TODO application/x-dtbncx+xml
        return self._handle_fallback(head, body, item)

    def _handle_xhtml(self, head, body, item):
        with item.open() as file:
            tree = parse_html(file)
        # TODO handle hrefs to other files (e.g. images, CSS...)
        # TODO probably should clean a bit?
        body.extend([
            etree.Comment(f' id={item.id!r} media-type={item.mime_type!r} '),
            '\n',
            *tree.find('body').getchildren(),
        ])
        return True

    def _handle_fallback(self, head, body, item):
        fallback = item.fallback
        if fallback:
            body.extend([
                etree.Comment(f' id={item.id!r} media-type={item.mime_type!r} -> not supported, falling back... '),
                '\n',
            ])
            return self._handle_item(head, body, fallback)
        body.extend([
            etree.Comment(f' id={item.id!r} media-type={item.mime_type!r} -> not supported, no fall back, ignored '),
            '\n',
        ])
        return False
