
from setuptools import find_packages, setup


setup(

    name = 'epub',
    version = '0.1.0',
    packages = find_packages(),

    author = 'Johan Berdat',
    author_email = 'jojolebarjos@gmail.com',
    license_file = 'LICENSE',

    url = 'https://gitlab.com/jojolebarjos/epub',

    description = 'minimalist EPUB parser',

    keywords = [
        'epub',
        'html',
    ],

    classifiers = [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: Freely Distributable',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Text Processing :: Linguistic'
    ],

    python_requires = '>=3.6',
    install_requires = [
        'beautifulsoup4',
        'lxml',
    ],

)
